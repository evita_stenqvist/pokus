//This file merely configures the store for hot reloading.
//This boilerplate file is likely to be the same for each project that uses Redux.
//With Redux, the actual stores are in /reducers.

import { createStore, compose } from 'redux';
import focusApp from '../reducers';

export default function configureStore(initialState) {
  let store = createStore(focusApp, initialState, compose(
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
    );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers').default;
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
