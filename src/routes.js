import React from 'react';
import { Route, IndexRoute } from 'react-router';

import AppWrapper from './components/AppWrapper';

export default (
		<Route path="/" component={AppWrapper} />
);
