/*eslint-disable import/default*/

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { applyRouterMiddleware, Router, browserHistory } from 'react-router';
import AppWrapper from './components/AppWrapper';
import configureStore from './store/configureStore';
import './styles/form.css';

const store = configureStore();

render(
	<Provider store={store}>
		<AppWrapper />
	</Provider>, document.getElementById('root')
);

//		render={applyRouterMiddleware(useScroll())} 